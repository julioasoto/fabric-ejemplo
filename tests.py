import micodigo

def test_suma():
    assert micodigo.funcion_suma(3,2)==5

def test_resta():
    assert micodigo.funcion_resta(3,2)==1
    
def test_va_a_fallar():
    assert micodigo.funcion_que_no_va_a_pasar_los_tests()==(-1)
