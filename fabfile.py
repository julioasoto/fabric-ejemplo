from fabric.api import env, run, local, settings

env.hosts=['192.168.1.150']

def acciones_remotas():
    with settings(user="usuario_remoto", password='password_remota'):
        print env
        run('ls -l')
        run('sudo apt-get update && sudo apt-get upgrade')
        local('ls -l')



# Ejemplo de como podemos hacer test, commit y push en un solo comando:

def comprobar():
    local("nosetests -v tests.py")

def commit(mensaje):
    local("git add . && git commit -m '%s'" % mensaje)

def push(branch="master"):
    local("git push origin %s" % branch)

def damecalo(mensaje, branch="master"):
    comprobar()
    commit(mensaje)
    push(branch)
